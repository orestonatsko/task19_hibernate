package com.epamcourses.orestonatsko.menu;

import com.epamcourses.orestonatsko.SessionManager;
import com.epamcourses.orestonatsko.model.AddressEntity;
import com.epamcourses.orestonatsko.model.DisciplineEntity;
import com.epamcourses.orestonatsko.model.GroupEntity;
import com.epamcourses.orestonatsko.model.StudentEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.*;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Command> methods;
    private Scanner input;


    public Menu() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methods = new HashMap<>();
        initMenu();

    }

    public void show() {
        String userInput;
        do {
            System.out.println("\n\t\t~~MENU~~");
            for (String key : menu.keySet()) {
                if (key.length() == 1) {
                    System.out.println(key + " - " + menu.get(key));
                }
            }
            System.out.println("Q - Quit");
            userInput = input.next();
            if (userInput.matches("^\\d")) {
                showSubMenu(userInput);
                userInput = input.next();
            }
            try {
                methods.get(userInput).execute();
            } catch (NullPointerException e) {
                //    ignore for cleaner console  view
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } while (!userInput.equalsIgnoreCase("Q"));
    }

    private void showSubMenu(String userInput) {
        System.out.println("\t~~TABLE MENU~~");
        for (String key : menu.keySet()) {
            if (key.length() != 1 && key.substring(0, 1).equals(userInput)) {
                System.out.println(key + " - " + menu.get(key));
            }
        }
    }

    private void initMenu() {

        //student table menu items
        menu.put("1", "Student Table");
        menu.put("11", "Get All Students");
        menu.put("12", "Get Student by Id");
        menu.put("13", "Get Student by Name");
        menu.put("14", "Add Student");
        menu.put("15", "Update Student");
        menu.put("16", "Delete Student");

        //address table menu items
        menu.put("2", "Address Table");
        menu.put("21", "Get All Addresses");
        menu.put("22", "Get Address by Id");
        menu.put("23", "Add Address");
        menu.put("24", "Update Address");
        menu.put("25", "Delete Address");

        //discipline table menu items
        menu.put("3", "Discipline Table");
        menu.put("31", "Get All Disciplines");
        menu.put("32", "Get DisciplineEntity by Id");
        menu.put("33", "Add DisciplineEntity");
        menu.put("34", "Update DisciplineEntity");
        menu.put("35", "Delete DisciplineEntity");

        //student group table menu items
        menu.put("4", "Student Group Table");
        menu.put("41", "Get All Disciplines");
        menu.put("42", "Get DisciplineEntity by Id");
        menu.put("43", "Add DisciplineEntity");
        menu.put("44", "Update DisciplineEntity");
        menu.put("45", "Delete DisciplineEntity");


        //operations with student able
        methods.put("11", this::findAllStudents);
        methods.put("12", this::getStudentById);
        methods.put("13", this::getStudentByName);
        methods.put("14", this::insertStudent);
//        methods.put("15", this::updateStudent);
        methods.put("16", this::deleteStudent);

        //operations with address able
        methods.put("21", this::getAllAddresses);
        methods.put("22", this::getAddressById);
        methods.put("23", this::addAddress);
        methods.put("24", this::updateAddress);
        methods.put("25", this::deleteAddress);

//        operations with discipline able
        methods.put("31", this::selectAllDisciplines);
        methods.put("32", this::getDisciplineById);
        methods.put("33", this::addDiscipline);
        methods.put("34", this::updateDiscipline);
        methods.put("35", this::deleteDiscipline);

//        operations with group able
        methods.put("41", this::findAllGroups);
//        methods.put("42", this::findGroupById);
//        methods.put("43", this::addGroup);
//        methods.put("44", this::updateUpdateGroup);
        methods.put("45", this::deleteGroup);
    }

    private void deleteGroup() {
        Session session = SessionManager.getSession();
        System.out.println("Enter ID group you want to delete:");
        int id = input.nextInt();
        session.beginTransaction();
        GroupEntity group = session.load(GroupEntity.class, id);
        Set<StudentEntity> students = new LinkedHashSet<>();
        Query stQuery = session.createQuery("FROM StudentEntity WHERE group=:group");
        stQuery.setParameter("group", group);
        for (Object obj : stQuery.list()) {
            students.add((StudentEntity) obj);
        }
        students.forEach(studentEntity -> {
            studentEntity.setGroup(null);
            session.update(studentEntity);
        });
        session.getTransaction().commit();
        System.out.println("~~STUDENT GROUP TABLE [group with id: " + id + " - deleted]~~");
    }

    private void showStudentTable(Set<StudentEntity> students) {
        System.out.println("\t\t\t\t\t\tTABLE" + " STUDENT");
        System.out.println(String.format("%-5s %-15s %-15s %-15s %-15s", "id", "first_name", "second_name",
                "numb_building", "group"));
        students.forEach(System.out::println);
    }

    private void findAllGroups() {
        Session session = SessionManager.getSession();
        Query query = session.createQuery("FROM GroupEntity");
        Set<GroupEntity> groups = new LinkedHashSet<>();
        for (Object obj : query.list()) {
            groups.add((GroupEntity) obj);
        }
        showGroupTable(groups);

    }

    private void showGroupTable(Set<GroupEntity> groups) {
        System.out.println("\t\tSTUDENT GROUP TABLE");
        System.out.println(String.format("%-5s %s", "ID", "group"));
        groups.forEach(System.out::println);
    }

    private void deleteStudent() {
        Session session = SessionManager.getSession();
        System.out.println("Enter ID of student you want to update:");
        int id = input.nextInt();
        session.beginTransaction();
        Query query = session.createQuery("DELETE StudentEntity WHERE id=:id");
        query.setParameter("id", id);
        int result = query.executeUpdate();
        session.getTransaction().commit();
        System.out.println("~~ STUDENT TABLE [Rows deleted: " + result + "}~~");

    }

    private void deleteDiscipline() {
        Session session = SessionManager.getSession();
        System.out.println("Enter ID of discipline you want to update:");
        int id = input.nextInt();
        session.beginTransaction();
        Query query = session.createQuery("DELETE DisciplineEntity WHERE id=:id");
        query.setParameter("id", id);
        int result = query.executeUpdate();
        session.getTransaction().commit();
        System.out.println("~~DISCIPLINE TABLE [Rows deleted: " + result + "]~~");
    }

    private void updateDiscipline() {
        Session session = SessionManager.getSession();
        System.out.println("Enter ID of discipline you want to update:");
        int id = input.nextInt();
        session.beginTransaction();
        System.out.println("Enter new name of discipline you want to update:");
        String newName = input.next();
        Query updateQuery = session.createQuery("UPDATE DisciplineEntity SET discipline=:newName WHERE id=:id");
        updateQuery.setParameter("newName", newName);
        updateQuery.setParameter("id", id);
        int result = updateQuery.executeUpdate();
        session.getTransaction().commit();
        System.out.println("~~DISCIPLINE TABLE [Rows updated: " + result + "]~~");
    }

    private void addDiscipline() {
        Session session = SessionManager.getSession();
        System.out.println("Enter name of discipline:");
        String nameOfDiscipline = input.next();
        DisciplineEntity discipline = new DisciplineEntity();
        discipline.setDiscipline(nameOfDiscipline);
        session.beginTransaction();
        session.save(discipline);
        session.getTransaction().commit();
        System.out.println("~~Discipline added~~");
    }

    private void getDisciplineById() {
        Session session = SessionManager.getSession();
        System.out.println("Enter id of discipline:");
        int id = input.nextInt();
        DisciplineEntity discipline = session.load(DisciplineEntity.class, id);
        showDisciplineTable(Arrays.asList(discipline));
    }

    private void selectAllDisciplines() {
        Session session = SessionManager.getSession();
        Query query = session.createQuery("FROM DisciplineEntity");
        List<DisciplineEntity> disciplines = new ArrayList<>();
        for (Object obj : query.list()) {
            disciplines.add((DisciplineEntity) obj);
        }
        showDisciplineTable(disciplines);
    }

    private void showDisciplineTable(List<DisciplineEntity> disciplines) {
        System.out.println("\t\t\tDISCIPLINE TABLE");
        System.out.println(String.format("%-5s %s", "ID", "discipline"));
        disciplines.forEach(System.out::println);
    }

    private void deleteAddress() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Delete address");
            System.out.println("Enter city:");
            String city = input.next();
            List<AddressEntity> addressList = new ArrayList<>();
            session.beginTransaction();
            Query query = session.createQuery("from AddressEntity where city=:city");
            query.setParameter("city", city);
            for (Object obj : query.list()) {
                addressList.add((AddressEntity) obj);
            }
            int result = 0;
            for (AddressEntity addressEntity : addressList) {
                int id = addressEntity.getId();
                Query deleteQuery = session.createQuery("delete AddressEntity where addressId=:code1");
                deleteQuery.setParameter("code1", id);
                result += deleteQuery.executeUpdate();
            }
            session.getTransaction().commit();
            System.out.println("Rows deleted: " + result);
        }
    }

    private void updateAddress() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Enter old address:");
            System.out.println("old city:");
            String oldCity = input.next();
            System.out.println("new city:");
            String newCity = input.next();
            session.beginTransaction();
            Query query = session.createQuery("from AddressEntity where city=:oldCity");
            query.setParameter("oldCity", oldCity);
            List<AddressEntity> addressList = new ArrayList<>();
            for (Object obj : query.list()) {
                addressList.add((AddressEntity) obj);
            }
            int result = 0;
            for (AddressEntity addressEntity : addressList) {
                Query updateQuery = session.createQuery("update AddressEntity set city=:code2 where addressId=:code1");
                updateQuery.setParameter("code1", addressEntity.getId());
                updateQuery.setParameter("code2", newCity);
                result += updateQuery.executeUpdate();
            }
            session.getTransaction().commit();
            System.out.println("Updated rows: " + result);
        }
    }

    private void addAddress() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Enter city:");
            String city = input.next();
            System.out.println("Enter street:");
            String street = input.next();
            System.out.println("Enter number of building:");
            int numbBuilding = input.nextInt();
            AddressEntity address = new AddressEntity();
            address.setCity(city);
            address.setStreet(street);
            address.setNumbBuilding(numbBuilding);
            session.beginTransaction();
            session.save(address);
            session.getTransaction().commit();
            System.out.println("~~Address added~~");
        }
    }

    private void insertStudent() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Enter first name:");
            String firstName = input.next();
            System.out.println("Enter second name");
            String secondName = input.next();
            StudentEntity student = new StudentEntity();
            student.setFirstName(firstName);
            student.setSecondName(secondName);
            session.beginTransaction();
            session.save(student);
            session.getTransaction().commit();
            System.out.println("Student added");
        }
    }

    private void getAddressById() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Enter address id:");
            int addressId = input.nextInt();
            AddressEntity address = session.load(AddressEntity.class, addressId);
            System.out.println(String.format("%-5s %-15s %-15s", "id", "street", "number"));
            System.out.println(address);
        }
    }


    private void getStudentByName() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Enter student name:");
            String studentName = input.next();
            Query query = session.createQuery("FROM StudentEntity WHERE firstName = :name");
            query.setParameter("name", studentName);
            List student = query.list();
//            StudentEntity student = session.load(StudentEntity.class, studentName);
            System.out.println(String.format("%-5s %-15s %-15s %-15s %-15s", "id", "first_name", "second_name",
                    "address", "group"));
            student.forEach(System.out::println);
        }
    }

    private void getStudentById() {
        try (Session session = SessionManager.getSession()) {
            System.out.println("Enter student id:");
            int studentId = input.nextInt();
            StudentEntity student = session.load(StudentEntity.class, studentId);
            System.out.println(String.format("%-5s %-15s %-15s %-15s %-15s", "id", "first_name", "second_name",
                    "address", "group"));
            System.out.println(student);
        }
    }

    private void getAllAddresses() {
        try (Session session = SessionManager.getSession()) {
            List<AddressEntity> addresses = new ArrayList<>();
            Query query = session.createQuery("from " + "AddressEntity ");
            for (Object obj : query.list()) {
                addresses.add((AddressEntity) obj);
            }
            System.out.println("\t\tTABLE" + " ADDRESS");
            System.out.println(String.format("%-5s %-15s %-18s %-15s", "id", "city", "street", "number"));
            addresses.forEach(System.out::println);
        }
    }

    private void findAllStudents() {
        try (Session session = SessionManager.getSession()) {
            Set<StudentEntity> students = new LinkedHashSet<>();
            Query query = session.createQuery("from " + "StudentEntity ");
            for (Object obj : query.list()) {
                students.add((StudentEntity) obj);
            }
            showStudentTable(students);
        }
    }

}