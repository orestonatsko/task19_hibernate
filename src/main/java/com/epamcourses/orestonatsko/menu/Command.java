package com.epamcourses.orestonatsko.menu;

import java.sql.SQLException;

public interface Command {
    void execute() throws SQLException;
}
