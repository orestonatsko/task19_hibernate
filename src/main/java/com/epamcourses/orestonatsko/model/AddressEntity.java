package com.epamcourses.orestonatsko.model;

import javax.persistence.*;

@Entity
@Table(name = "address",schema = "student_db")
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Integer addressId;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "numb_building")
    private Integer numbBuilding;

    public AddressEntity() {
    }

    public Integer getId() {
        return addressId;
    }

    public void setId(Integer id) {
        this.addressId = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getNumbBuilding() {
        return numbBuilding;
    }

    public void setNumbBuilding(Integer numbBuilding) {
        this.numbBuilding = numbBuilding;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-18s %-15d", addressId, city, street, numbBuilding);
    }
}
