package com.epamcourses.orestonatsko.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "discipline", schema = "student_db")
public class DisciplineEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "discipline_id", nullable = false)
    private Integer id;
    @Column(name = "discipline")
    private String discipline;

    @ManyToMany(mappedBy = "disciplineList")
    private Set<StudentEntity> students;

    public DisciplineEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    @Override
    public String toString() {
        return String.format("%-5d %s", id, discipline);
    }
}
