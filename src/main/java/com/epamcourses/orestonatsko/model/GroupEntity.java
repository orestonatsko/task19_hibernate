package com.epamcourses.orestonatsko.model;

import javax.persistence.*;

@Entity
@Table(name = "st_group", schema = "student_db")
public class GroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private Integer groupId;

    @Column(name = "name")
    private String name;

    public GroupEntity() {
    }

    public Integer getId() {
        return groupId;
    }

    public void setId(Integer id) {
        this.groupId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-5d %s", groupId, name);
    }
}
