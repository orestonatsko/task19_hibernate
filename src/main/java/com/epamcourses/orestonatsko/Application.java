package com.epamcourses.orestonatsko;

import com.epamcourses.orestonatsko.menu.Menu;

public class Application {

    public static void main(String[] args) {
        new Menu().show();
    }
}
